import {typelist} from '../type';

const intialstate={
    friendslist:[]
}

export default function(state=intialstate,action){
    switch (action.type) {
        case typelist:
            return {
                ...state,
                friendslist:action.payload
            }
            
    
        default: return state;

         
    }
}