import React from "react";
import { Route, Switch,BrowserRouter } from "react-router-dom";
import Main from "../components/Main";
import Next from "../components/Next";
import Listfriends from "../components/Listfriends";

class Routes extends React.Component {
  render() {
    return (
        <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Main} />
        <Route path="/next" exact component={Next} />
        <Route path="/friendslist" exact component={Listfriends} />
      </Switch>
      </BrowserRouter>
    );
  }
}

export default Routes;
