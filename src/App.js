import React from 'react';
import Route from './route/route';
import {Provider} from 'react-redux';
import store from './redux/store';

class App extends React.Component{
  render(){
  return(
    <Provider store={store}>
    
      <Route/>

    </Provider>
  );
}
}

export default App;
