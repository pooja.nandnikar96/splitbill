import React from "react";
import {
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { Link } from "react-router-dom";

class Listfriends extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalopen: false,
      friend:''
    };
  }
  toggle = () => {
    const { modalopen } = this.state;
    this.setState({ modalopen: !modalopen });
  };

  addfriend = () => {
      const {friend,modalopen}=this.state;
const requestobj={
    friendname:friend
}
  }

  handlechange = (e) => {
    const {name,value}=e.target
  this.setState({ [name]: value },()=>{
console.log('updated',this.state);

  });
}
  render() {
    const { modalopen } = this.state;
    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>Sl.No</th>
              <th>Friends Names</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Pooja</td>
              <td>Remove</td>
            </tr>
          </tbody>
        </Table>
        <Button onClick={this.toggle}>Add</Button>
        <div>
          <Modal isOpen={modalopen} toggle={this.toggle}>
            <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
            <ModalBody>
              <input
                type="text"
                name="friend"
                placeholder="enter your friend name"
                onChange={this.handlechange}
              />
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.addfriend}>
                Add Friend
              </Button>{" "}
              <Button color="secondary" onClick={this.toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    );
  }
}

export default Listfriends;
