import React from "react";
import "./Main.css";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: "",
      number: "",
      cal:''
    };
  }

  calculate = () => {
      const {amount,number,cal}=this.state;
      const calc=(amount/number);
      this.setState({cal:calc},()=>{
          console.log(cal);
      });
     
  }

  

  handlechange = (e) => {
      const {name,value}=e.target
    this.setState({ [name]: value },()=>{
console.log('updated',this.state);

    });
  }

  render() {
      const {cal,amount,number}=this.state;
    return (

      <div class="maindiv">
        <label>Enter total amount:</label>
        <input type="text" name="amount" onChange={this.handlechange}/>
        <br />

        <label>Enter number of Persons:</label>
        <input type="text" name="number" onChange={this.handlechange}/>
        <br />

        <input type="button" value="calculate" onClick={this.calculate} />
        {
        cal>0?
        <div>For {number} persons, total amount is {amount}. So each person shares {cal}</div>:
        <div>no value</div>
        }
      </div>
    );
  }
}

export default Main;
